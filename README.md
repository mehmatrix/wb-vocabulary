# Vocabulary Application
> Nextjs, ReactJS, Eslint (Airbnb), Fastify, LocalStorage, Server Side Rendering

## Setup

```
git clone git@gitlab.com:mehmatrix/wb-vocabulary.git
cd wb-vocabulary
npm install
npm start
```
## Test

```
npm test
```
    
## Notes

- I used nextjs to avoid writing configurations and to demonstrate server side rendering.
- I used fastify to create a basic http server.
- I stored words in memory on server side and in LocalStorage on clientSide.
- App can work offline as well, but i didn't handle data sync when it gets back online.
- I'm planning to convert this app to be progressive web app in future :D
- I didn't use Redux since it's a very basic application.
- I just wrote one single test to demonstrate how to test react components.