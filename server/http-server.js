const fastify = require('fastify');
const wordData = require('./data');

const port = parseInt(process.env.PORT, 10) || 3000;
const server = fastify();

const createServer = (handle) => {
  let words = wordData;

  server.get('/api/words', (req, reply) => {
    reply.send(words);
  });

  server.post('/api/word', (req, reply) => {
    const { word } = req.body;
    words.push(word);
    reply.send(word);
  });

  server.delete('/api/word/:wordId', (req, reply) => {
    const { wordId } = req.params;
    words = words.filter(({ id }) => id !== wordId);
    reply.send({ wordId });
  });

  server.get('/*', ({ req }, { res }) => handle(req, res));

  server.listen(port, (err) => {
    if (err) throw err;
    console.log(`> Ready on http://localhost:${port}`);
  });

  return server;
};

module.exports = {
  server, // for testing
  createServer,
};
