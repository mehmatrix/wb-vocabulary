const uniqid = require('uniqid');

const words = [
  {
    original: 'One',
    translation: 'Uno',
  },
  {
    original: 'Two',
    translation: 'Dos',
  },
  {
    original: 'Three',
    translation: 'Tres',
  },
  {
    original: 'Four',
    translation: 'Cuatro',
  },
  {
    original: 'Five',
    translation: 'Cinco',
  },
  {
    original: 'Six',
    translation: 'Seis',
  },
];

module.exports = words.map(word => ({ id: uniqid(), ...word }));
