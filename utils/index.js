export const cloneObj = obj => JSON.parse(JSON.stringify(obj));

export const storeWords = words => window.localStorage.setItem('words', JSON.stringify(words));

export const getWords = () => {
  const words = window.localStorage.getItem('words');

  if (!words) return [];

  return JSON.parse(words);
};

export const shuffleArray = (a) => {
  for (let i = a.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
};

export const initStorage = (words) => {
  /**
   * If the backend has different length of words,
   * overwrite localStorage
   */
  if (!getWords() || getWords().length !== words.length) {
    storeWords(words);
  }
};
