import React from 'react';
import PropTypes from 'prop-types';

const Results = ({ answers, correctAnswerCount, questionCount }) => {
  const renderRows = () => answers.map(({
    id, answer, word, isCorrect,
  }) => (
    <tr key={id}>
      <td>{word.original}</td>
      <td>{word.translation}</td>
      <td className={isCorrect ? 'is-success' : 'is-danger'}>{answer}</td>
    </tr>
  ));

  return (
    <div>
      <h1>{`Results: ${correctAnswerCount} / ${questionCount} (${Math.round((correctAnswerCount / questionCount) * 100)}%)`}</h1>
      <table className="table table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <thead>
          <tr>
            <th>Original</th>
            <th>Translation</th>
            <th>Your Answer</th>
          </tr>
        </thead>
        <tbody>
          {renderRows()}
        </tbody>
      </table>
    </div>
  );
};

Results.propTypes = {
  answers: PropTypes.array.isRequired,
  correctAnswerCount: PropTypes.number.isRequired,
  questionCount: PropTypes.number.isRequired,
};
Results.defaultProps = {};

export default Results;
