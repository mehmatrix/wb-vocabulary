import React from 'react';
import PropTypes from 'prop-types';
import Question from './Question';

const Test = ({
  answersCount,
  questionCount,
  word,
  submitAnswer,
}) => (
  <React.Fragment>
    <h1>Progress {`${answersCount} / ${questionCount}`}</h1>
    <progress
      className="progress is-success"
      value={answersCount}
      max={questionCount}>
      {`${answersCount} / ${questionCount}`}
    </progress>
    <div>
      <p><strong>{`Word ${answersCount + 1}: `}</strong>{word.original}</p>
      <Question submitAnswer={submitAnswer} />
    </div>
  </React.Fragment>
);

Test.propTypes = {
  answersCount: PropTypes.number.isRequired,
  questionCount: PropTypes.number.isRequired,
  word: PropTypes.object.isRequired,
  submitAnswer: PropTypes.func.isRequired,
};
Test.defaultProps = {};

export default Test;
