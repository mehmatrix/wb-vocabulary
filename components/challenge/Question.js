import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Question extends Component {
  constructor() {
    super();

    this.state = {
      answer: '',
    };
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };


  onSubmit = (e) => {
    e.preventDefault();

    this.props.submitAnswer(this.state);
    this.setState({ answer: '' });
  };

  render() {
    return (
      <form onSubmit={this.onSubmit}>
        <div className="field">
          <div className="control">
            <input
              name="answer"
              className="input"
              type="text"
              placeholder="Answer"
              value={this.state.answer}
              onChange={this.onChange} />
          </div>
        </div>
        <button className="button" onClick={this.onSubmit}>
          Submit Answer
        </button>
      </form>
    );
  }
}

Question.propTypes = {
  submitAnswer: PropTypes.func.isRequired,
};

export default Question;
