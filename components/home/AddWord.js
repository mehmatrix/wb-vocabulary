import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { cloneObj } from '../../utils';

const initialState = {
  original: '',
  translation: '',
};

class AddWord extends Component {
  constructor() {
    super();

    this.state = cloneObj(initialState);
  }

  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };


  onSubmit = (e) => {
    e.preventDefault();

    this.props.submitNewWord(this.state);
    this.setState(cloneObj(initialState));
  };

  render() {
    return (
      <form style={{ marginBottom: 30 }} onSubmit={this.onSubmit}>
        <div className="field">
          <label htmlFor="original" className="label">Original</label>
          <div className="control">
            <input
              id="original"
              name="original"
              className="input"
              type="text"
              placeholder="Original Text"
              value={this.state.original}
              onChange={this.onChange} />
          </div>
        </div>
        <div className="field">
          <label className="label">Translation</label>
          <div className="control">
            <input
              id="translation"
              name="translation"
              className="input"
              type="text"
              placeholder="Translation"
              value={this.state.translation}
              onChange={this.onChange} />
          </div>
        </div>
        <button type="submit" className="button">
          Add Word
        </button>
      </form>
    );
  }
}

AddWord.propTypes = {
  submitNewWord: PropTypes.func.isRequired,
};

export default AddWord;
