import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import Link from 'next/link';

const Layout = ({ children }) => (
  <React.Fragment>
    <Head>
      <title>PWA Vocabulary</title>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css" />
    </Head>
    <div className="container">
      <div className="tabs">
        <ul>
          <li><Link href="/"><a href="/">Words</a></Link></li>
          <li><Link href="/challenge"><a href="/challenge">Challenge Mode</a></Link></li>
        </ul>
      </div>
    </div>
    {children}
  </React.Fragment>
);

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
