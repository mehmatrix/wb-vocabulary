import React from 'react';
import renderer from 'react-test-renderer';
import WordsPage from '../../pages/index';
import LocalStorageMock from '../localStorageMock';
import words from '../../server/data';

global.localStorage = new LocalStorageMock();
global.sessionStorage = new LocalStorageMock();

test('WordsPage snapshot without words', () => {
  const componentRenderer = renderer.create(<WordsPage words={[]} />);
  const tree = componentRenderer.toJSON();
  expect(tree).toMatchSnapshot();
});

test('WordsPage snapshot with words', () => {
  const componentRenderer = renderer.create(<WordsPage words={words} />);
  const tree = componentRenderer.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Wordspage should render the table with words', () => {
  const componentRenderer = renderer.create(<WordsPage words={words} />);
  const componentInstance = componentRenderer.root;

  expect(componentInstance.findByType('tbody').children.length).toEqual(words.length);
});
