const next = require('next');
const { createServer } = require('./server/http-server');

const dev = process.env.NODE_ENV !== 'production';
const nextApp = next({ dev });
const handle = nextApp.getRequestHandler();

const run = async () => {
  await nextApp.prepare();
  createServer(handle);
};

run().then(() => console.log('Started!'));
