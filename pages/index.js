import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import uniqid from 'uniqid';

import Layout from '../components/Layout';
import AddWord from '../components/home/AddWord';
import { initStorage, storeWords } from '../utils';

class WordsPage extends Component {
  static async getInitialProps() {
    const response = await axios.get('http://localhost:3000/api/words');
    return { words: response.data };
  }

  constructor(props) {
    super(props);

    this.state = {
      words: props.words,
    };
  }

  componentDidMount() {
    initStorage(this.props.words);
  }

  addWord = (word) => {
    word.id = uniqid();
    axios.post('/api/word', { word });

    this.state.words.push(word);
    this.setState(this.state, () => { storeWords(this.state.words); });
  };

  deleteWord = (wordId) => {
    this.state.words = this.state.words.filter(({ id }) => wordId !== id);
    this.setState(this.state, () => { storeWords(this.state.words); });
    axios.delete(`/api/word/${wordId}`);
  };

  renderRows = () => this.state.words.map(({ original, translation, id }) => (
    <tr key={id}>
      <td>{original}</td>
      <td>{translation}</td>
      <td><button onClick={() => this.deleteWord(id)}>Delete</button></td>
    </tr>
  ));

  render() {
    return (
      <Layout>
        <div className="container" style={{ marginTop: '30px' }}>
          <div className="columns">
            <div className="column is-one-third">
              <h1>Add Word</h1>
              <AddWord submitNewWord={this.addWord} />
            </div>
            <div className="column is-two-thirds">
              <h1>Words</h1>
              <table className="table table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
                <thead>
                  <tr>
                    <th>Original</th>
                    <th>Translation</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {this.renderRows()}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

WordsPage.propTypes = {
  words: PropTypes.array.isRequired,
};

export default WordsPage;
