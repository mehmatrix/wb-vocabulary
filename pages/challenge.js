import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uniqid from 'uniqid';
import axios from 'axios';
import Layout from '../components/Layout';
import { initStorage, getWords, shuffleArray } from '../utils';
import Results from '../components/challenge/Results';
import Test from '../components/challenge/Test';

let QUESTION_COUNT = 5;

class ChallengePage extends Component {
  static async getInitialProps() {
    const response = await axios.get('http://localhost:3000/api/words');
    return { words: response.data };
  }

  constructor(props) {
    super(props);

    this.state = {
      words: [],
      answers: [],
      correctAnswerCount: 0,
    };
  }

  componentDidMount() {
    initStorage(this.props.words);

    let words = getWords();
    if (words && words.length > 0) {
      if (words.length < QUESTION_COUNT) {
        QUESTION_COUNT = words.length;
      }

      words = shuffleArray(words).slice(0, QUESTION_COUNT);
      this.setState({
        words,
      });
    }
  }

  submitAnswer = ({ answer }) => {
    const { words, answers } = this.state;
    const word = words[answers.length];

    const isCorrect = word.translation.toLowerCase() === answer.toLowerCase();

    this.state.answers.push({
      id: uniqid(),
      word,
      answer,
      isCorrect,
    });

    if (isCorrect) {
      this.state.correctAnswerCount += 1;
    }

    this.setState(this.state);
  };

  renderResults = () => (
    <Results
      answers={this.state.answers}
      correctAnswerCount={this.state.correctAnswerCount}
      questionCount={QUESTION_COUNT} />
  );

  renderTest = () => {
    const { words, answers } = this.state;

    if (words.length === 0 || answers.length === QUESTION_COUNT) {
      return null;
    }

    return (
      <Test
        word={words[answers.length]}
        answersCount={answers.length}
        questionCount={QUESTION_COUNT}
        submitAnswer={this.submitAnswer} />
    );
  };

  render() {
    const answersCount = this.state.answers.length;

    return (
      <Layout>
        <div className="container" style={{ marginTop: '30px' }}>
          { answersCount === QUESTION_COUNT ?
            this.renderResults() : this.renderTest()
          }
        </div>
      </Layout>
    );
  }
}

ChallengePage.propTypes = {
  words: PropTypes.array.isRequired,
};

export default ChallengePage;
